import jdk.nashorn.internal.runtime.arrays.ArrayIndex;

import java.util.ArrayList;
import java.util.Scanner;

public class testPurpBox {

    public static void main(String args[]) throws Exception {

        Scanner sc = new Scanner(System.in);

        ArrayList<Movie> movies = new ArrayList<>();
        PurpAdmin purpAdmin = new PurpAdmin(movies);
        PurpBox purpBox = new PurpBox(movies);

        //Display input options to user
        System.out.println("1. Enter 1 for Admin \n2. Enter 2 for User");


        //Accept input from user
        int choice = sc.nextInt();

        switch (choice){

            //Admin Stuff
            case 1:

                System.out.println("1. Add a Movie in the Cart \n2. Remove Movie from inventory \n3. Change Price  \n4. Volume discount \n5. Add Promo Codes  \n6. Disable Unit \n7. Enable Unit");

                int adminChoice = sc.nextInt();

                switch (adminChoice){

                    //Add Movie to Cart
                    case 1:

                        System.out.println("Enter movie name: ");
                        String movieName = sc.nextLine();

                        System.out.println("Enter release date: ");
                        String releaseDate = sc.nextLine();

                        System.out.println("Enter genre of movie: ");
                        String genre = sc.nextLine();

                        System.out.println("Enter movie box office gross: ");
                        double price  = sc.nextFloat();

                        System.out.println("Is the movie in Blue Ray? (Yes or No): ");
                        String isBlueRay = sc.nextLine();
                        boolean isBlueray;

                        System.out.println("Enter quantity of movie: ");
                        int quantity = sc.nextInt();

                        System.out.println("Is the movie enabled? (Yes or No: ");
                        String isEnbl = sc.nextLine();
                        boolean isEnabled;

                        isBlueray = isBlueRay.equals("Yes");
                        isEnabled = isEnbl.equals("Yes");

                        Movie movie = new Movie(movieName,quantity,genre,isBlueray,releaseDate,price,isEnabled);
                        purpAdmin.addMovieToInventory(movie);

                        System.out.println("Movie Saved Successfully");
                        break;

                    //Delete Movie from inventory
                    case 2:

                        System.out.println("Enter title to delete: ");
                        String delete = sc.nextLine();

                        Movie movie1 = new Movie();
                        movie1.setTitle(delete);
                        purpAdmin.removeFromInventory(movie1);

                        System.out.println("Movie Deleted Successfully");

                        break;

                    //Change Prices
                    case 3:
                        System.out.println("Enter new price : ");
                        double newPrice = sc.nextDouble();

                        purpAdmin.changePrices(newPrice);
                        System.out.println("Price changed Successfully");
                        break;

                    //Volume Discount
                    case 4:

                        System.out.println("Enter percentage discount: ");
                        int newDiscount  = sc.nextInt();

                        purpAdmin.volumeDiscount(newDiscount);

                        System.out.println("Discount Applied Successfully");

                        break;

                    //Add Promo Codes
                    case 5:

                        System.out.println("Enter new promo code: ");
                        String promoCode = sc.nextLine();

                        purpAdmin.addPromoCode(promoCode);

                        System.out.println("Promo Code added Successfully");
                        break;

                    //Disable Unit
                    case 6:
                        System.out.println("Enter unit title to disable: ");
                        String title = sc.nextLine();

                        purpAdmin.isEnable(false, title);

                        break;

                    //Enable Unit
                    case 7:
                        System.out.println("Enter unit title to enable: ");
                        String titles = sc.nextLine();

                        purpAdmin.isEnable(true, titles);
                        break;

                    default:
                        System.out.println("Wrong Input try again");
                        break;

                }

                break;

            case 2:

                System.out.println("1. See all movies \n2. See all BlueRay movies \n3. See all DvD movies \n4. Search movie by genre \n5. Add Promo Codes  \n6. Disable Unit \n7. Enable Unit");

                int userChoice = sc.nextInt();

                switch(userChoice){

                    //See All Movies
                    case 1:

                        purpBox.seeAllMovies(movies);
                        break;

                    //Search BlueRay
                    case 2:

                        purpBox.searchBluRay();
                        break;

                    //Search all DVD movies
                    case 3:

                        purpBox.searchDvD();
                        break;

                    //Search movie by genre
                    case 4:

                        System.out.println("Enter genre to search for movie: ");
                        String genre = sc.nextLine();

                        purpBox.searchGenre(genre);

                        break;

                    //Add Movie to Cart
                    case 5:
                        System.out.println("Enter title of movie to add to cart: ");
                        String title = sc.nextLine();

                        Movie movie = new Movie();
                        movie.setTitle(title);

                        purpBox.addMovie(movie);

                        break;

                    //Remove movie from cart
                    case 6:
                        System.out.println("Enter title of movie to add to cart: ");
                        String removeTitle = sc.nextLine();

                        Movie remMovie = new Movie();
                        remMovie.setTitle(removeTitle);

                        purpBox.removeFromCart(remMovie);

                        break;

                    //Remove All movies from cart
                    case 7:

                        purpBox.removeAllFromCart();
                        System.out.println("All Movies removed from cart successfully");
                        break;

                    //See if movie is available
                    case 8:
                        System.out.println("Enter title of movie to see if it's available: ");
                        String seeTitle = sc.nextLine();

                        Movie seeMovie = new Movie();
                        seeMovie.setTitle(seeTitle);

                        purpBox.checkAvailability(seeMovie);
                        break;

                    //Checkout movies from cart
                    case 9:
                        System.out.println("Checking out cart....");

                        purpBox.checkOutMovie();

                        System.out.println("Check out successful");
                        break;

                    //Return movies
                    case 10:
                        System.out.println("Enter title of movie to return it: ");
                        String returnTitle = sc.nextLine();

                        Movie returnMovie  = new Movie();
                        returnMovie.setTitle(returnTitle);

                        purpBox.returnMovie(returnMovie);
                        break;

                    //Apply promo code
                    case 11:
                        System.out.println("Enter promo code: ");
                        String promoCode = sc.nextLine();

                        purpBox.applyCode(promoCode);
                        break;

                        default:
                            System.out.println("Wrong Input try again");
                            break;
                }

                break;

            default:
                System.out.println("Wrong Input try again");
                break;




        }

    }
}