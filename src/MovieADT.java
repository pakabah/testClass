
public interface MovieADT {
	/*
	 Phase 1:
	 Group Project for Interface Files
	 Names: Itamar Marques, Raul Alderete, Esmir Osmanovic, Joe Hernandez
	 Jean Noel, Michael Riley, Ryan Mewhorter
	 */
	 
	/** 
	 * @require object with movie title exists 
	 * @ensure movie title in type String is returned 
	 */  
	   public String getTitle(); 
	  
	/** 
	 * @require: title must be a String 
	 * @ensure: Sets the title attribute of the movie class 
	 */ 
	   public void setTitle(String title);  
	 
	/** 
	 * @ensure: Returns the genre attribute of the movie class 
	 */ 
	   public String getGenre(); 
	 
	/** 
	 * @require genre in type string to be set 
	 * @ensure Genre is set by String that is passed through 
	 *  
	 */ 
	   public void setGenre(String genre); 
	 
	/** 
	 * @require 
	 * @ensure returns a Date object representing the movie was released 
	 */ 
	   public String getReleaseDate(); 
	 
	/** 
	 * @require : Movie hasDate 
	 * @ensure: valid date  
	 */ 
	   public void setReleaseDate(String date); 
	 
	/** 
	 * @require: movie is either bluray or not 
	 * @ensure:  true | false 
	 *  
	 */ 
	   public boolean getIsBluRay(); 
	 
	/** 
	 * @require: Movie object  
	 * @ensure:  isBluray = 0 | 1 
	 *  
	 */ 
	   public void setIsBluRay(boolean bluRay); 
	 
	/** 
	 * @require: price >= 0  
	 * @ensure: price is not negative  
	 *  
	 */ 
	   public double getPrice(); 
	 
	/** 
	 * @require price must be type double >= 0 
	 * @ensure price is set with type double pass through 
	 */ 
	   public void setPrice(double price); 
	 
	/** 
	 * @ensure quantity of movie object is returned in type int 
	 */ 
	   public int getQuantity(); 
	 
	/** 
	 * @require: quantity >= 0 
	 * @ensure:  valid positive integer 
	 */ 
	   public void setQuantity(int quantity); 
	 
	/** 
	 * @ensure String representation of Movie is returned 
	 */ 
	   public String toString(); 
	     
	 


}
