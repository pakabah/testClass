import java.util.ArrayList;

public class PurpBox implements PurpleBoxUserInterface {

    private ArrayList<Movie> cart = new ArrayList<>();

    private ArrayList<Movie> movies = new ArrayList<>();

    PurpBox(ArrayList<Movie> movies){
        this.movies = movies;
    }

    @Override
    public ArrayList<Movie> seeAllMovies(ArrayList<Movie> movies) {
        this.movies = movies;
        int i = 0;
        for (Movie movie : this.movies) {
            i++;
            System.out.println(i+". "+ movie.getTitle());
        }

        return null;
    }

    @Override
    public ArrayList<Movie> searchGenre(String movieGenre) {

        int i = 0;
        for (Movie movie : this.movies) {
            if(movieGenre.equals(movie.getGenre())){
                i++;
                System.out.println(i+". "+ movie.getTitle());
            }
        }

        return null;
    }

    @Override
    public ArrayList<Movie> searchNewReleases(String date) {

        int i = 0;
        for (Movie movie : this.movies) {
            if(date.equals(movie.getReleaseDate())){
                i++;
                System.out.println(i+". "+ movie.getTitle());
            }
        }
        return null;
    }

    @Override
    public ArrayList<Movie> searchBluRay() {

        int i = 0;
        for (Movie movie : this.movies) {
            if(movie.getIsBluRay()){
                i++;
                System.out.println(i+". "+ movie.getTitle());
            }
        }
        return null;
    }

    @Override
    public ArrayList<Movie> searchDvD() {

        int i = 0;
        for (Movie movie : this.movies) {
            if(!movie.getIsBluRay()){
                i++;
                System.out.println(i+". "+ movie.getTitle());
            }
        }
        return null;
    }

    @Override
    public void addMovie(Movie movieName) {
        cart.add(movieName);
    }

    @Override
    public void removeFromCart(Movie movie) {
        cart.remove(movie);
    }

    @Override
    public void removeAllFromCart() {
        for(int i=0;i<cart.size();i++){
            cart.remove(i);
        }
    }

    @Override
    public boolean checkAvailability(Movie movie) {
        return false;
    }

    @Override
    public void checkOutMovie() {
        for(int i=0;i<cart.size();i++){
            cart.remove(i);
        }
    }

    @Override
    public void returnMovie(Movie returnMovie) {

    }

    @Override
    public void applyCode(String applyCode) {

    }
}
