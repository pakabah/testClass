import java.util.ArrayList;

public class PurpAdmin implements AdminADT {

    private ArrayList<Movie> movies = new ArrayList<>();

    public ArrayList<String> promoCodes = new ArrayList<>();

    PurpAdmin( ArrayList<Movie> movies){
        this.movies = movies;
    }

    @Override
    public void addMovieToInventory(Movie addMovieToInv) {
        movies.add(addMovieToInv);
    }

    @Override
    public void removeFromInventory(Movie removeFromInv) {
        movies.remove(removeFromInv);
    }

    @Override
    public void changePrices(double changePrice) {
        for (Movie movie : movies) {
            System.out.println("Price = " + movie.getPrice());
            movie.setPrice(changePrice);
        }
    }

    @Override
    public void volumeDiscount(int volDiscount) {
        float discount = volDiscount / 100;

        for (Movie movie : movies) {
            double discountPrice = movie.getPrice() * discount;
            System.out.println("Discount Price = " + discountPrice);
            movie.setPrice(discountPrice);
        }
    }

    @Override
    public void addPromoCode(String addPromo) {
        promoCodes.add(addPromo);
    }

    @Override
    public boolean isEnable(boolean isEnable, String title) {

        for (Movie movie : movies) {
            if(title.equals(movie.getTitle())){
                return movie.isEnabled();
            }
        }
        return false;
    }
}
