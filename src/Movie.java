public class Movie implements MovieADT{

    private String title;
    private String genre;
    private String releaseDate;
    private double price;
    private int quantity;
    private boolean blueRay;
	private boolean isEnabled;
    

	public Movie(String title, int quantity, String genre, boolean BluRay, String releaseDate,double price,boolean isEnabled) {
		this.title = title;
		this.quantity = quantity;
		this.genre = genre;
		this.price = price;
		this.releaseDate = releaseDate;
		this.blueRay = BluRay;
		this.isEnabled = isEnabled;
	}

	public Movie(String title, String genre, boolean BluRay, String releaseDate,double price) {
		this.title = title;
		this.genre = genre;
		this.price = price;
		this.releaseDate = releaseDate;
		this.blueRay = BluRay;
	}

	public Movie(){

	}

	public boolean isEnabled() {
		return isEnabled;
	}

	public void setEnabled(boolean enabled) {
		isEnabled = enabled;
	}

	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getGenre() {
		return genre;
	}

	
	public void setGenre(String genre) {
		this.genre = genre;
	}


	public  String getReleaseDate() {
		return releaseDate;
	}

	
	public void setReleaseDate(String date) {
		this.releaseDate = date;
	}

	
	public boolean getIsBluRay() {
		return this.blueRay;
	}

	
	public void setIsBluRay(boolean bluRay) {
		this.blueRay = bluRay;
	}

	
	public double getPrice() {
		return price;
	}

	
	public void setPrice(double price) {
		this.price = price;
		
	}


	public int getQuantity() {
		return quantity;
	}

	
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

}
