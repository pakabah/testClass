import java.util.ArrayList;

/*
 *Phase 1:
 *Group Project for Interface Files
 *Names: Itamar Marques, Raul Alderete, Esmir Osmanovic, Joe Hernandez
 *Jean Noel, Michael Riley, Ryan Mewhorter
 */

public interface PurpleBoxUserInterface{ 
		 
		/** 
		* @require  Movie object needs a title 
		* @ensure String of movie title is returned  
		*/ 
		public ArrayList<Movie> seeAllMovies(ArrayList<Movie> movies);
		 
		/** 
		* @require Movie object must need genre  
		* @ensure String of movie genre is returned  
		*/ 
		public ArrayList<Movie> searchGenre(String movieGenre);

		/**
		* @require  
		* @ensure  Returns all new releases to the user 
		*/ 
		public ArrayList<Movie> searchNewReleases(String date);
		
		/** 
		* @require  search for Blu ray passing in the array
		* @ensure  Returns the list of bluray movies
		*/ 
		public ArrayList<Movie> searchBluRay();

		/**
	 	* @require  search for Blu ray passing in the array
		 * @ensure  Returns the list of bluray movies
	 	*/
		public ArrayList<Movie> searchDvD();

	/**
		* @require User adding a movie to the cart 
		* @ensure  
		*/ 
		public void addMovie(Movie movieName); 
		 
		/** 
		* @require adding movie to the user list 
		* @ensure User will have movies in the cart to check out 
		*/ 
		public void removeFromCart(Movie movie); 
		 
		/** 
		* @require  User removing all movies from the cart 
		* @ensure  all movie objects are removed from cart 
		*/ 
		public void removeAllFromCart(); 
		 
		/** 
		* @require User checks what's available 
		* @ensure Returns all available movies to the user 
		*/ 
		
		public boolean checkAvailability(Movie movie); 
		 
		/** 
		* @require  
		* @ensure entire cart is checked out to user 
		*/ 
		public void checkOutMovie();
		 
		/** 
		* @required  
		* @ensure User returns all movies rented 
		*/ 
		public void returnMovie(Movie returnMovie); 
		 
		/** 
		* @require Apply a certain data type to retrieve a promotional code 
		* @ensure Applying code to give a client the discount 
		*/ 
		public void applyCode(String applyCode); 
		 
		 
		} 
		 