
public interface AdminADT {
	/*
	 Phase 1: 
	 Group Project for Interface Files
	 Names: Itamar Marques, Raul Alderete, Esmir Osmanovic, Joe Hernandez
	        Jean Noel, Michael Riley, Ryan Mewhorter
	 */
	 
	/** 
	 * @require need to go through Movie Object 
	 * @ensure adding a Movie Object to Movie Inventory 
	 */  
	public void addMovieToInventory(Movie addMovieToInv); 
	 
	 
	/** 
	 * @require name of movie to be removed 
	 * @ensure move is removed from inventory list 
	 */ 
	public void removeFromInventory(Movie removeFromInv); 
	 
	/** 
	 * @require Type double to change price of movie 
	 * @ensure movie price is changed 
	 */ 
	public void changePrices(double changePrice); 
	 
	/** 
	 *  @require integer amount of movies for discount 
	 *  @ensure discount applied to user's cart 
	*/ 
	public void volumeDiscount(int volDiscount); 
	 
	/** 
	 *  @require string to use as promo code 
	 *  @ensure promo code is created that client is able to apply to cart 
	*/ 
	public void addPromoCode(String addPromo); 
	 
	/** 
	 *  @require boolean to disable/enable unit 
	 *  @ensure cart is disabled/enabled 
	*/ 
	public boolean isEnable(boolean isEnable, String title);

}
